def vowel_swapper(string):
    # ==============
    a = string.replace("a", "4",2).replace("A", "4",2).replace("e", "3",2).replace("E", "3",2).replace("i", "!",2).replace("I", "!",2).replace("o", "ooo",2).replace("O", "000",2).replace("u", "|_|",2).replace("U", "|_|",2)
    return(a)



    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
